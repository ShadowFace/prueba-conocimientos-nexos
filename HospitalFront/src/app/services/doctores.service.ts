import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Doctores } from '../interfaces/doctores';
import { Pacientes } from '../interfaces/pacientes';
import { ListaPacienteDoctor } from '../interfaces/lista-paciente-doctor';

const URL = environment.URL;

@Injectable({
  providedIn: 'root'
})
export class DoctoresService {

  constructor(private http: HttpClient) { }

  // traer todos los doctores
  getDoctores(): Observable<Doctores[]>{
    return this.http.get<Doctores[]>(`${ URL }/Doctores`);
  }

  // traer todos los pacientes
  getPacientes(): Observable<Pacientes[]>{
    return this.http.get<Pacientes[]>(`${ URL }/Pacientes`);
  }

  // crear doctor
  // tslint:disable-next-line: typedef
  crearDoctor(doctor: Doctores){
    return new Promise<Doctores>((resolve, reject) => {
      this.http.post<Doctores>(`${ URL }/Doctores`, doctor)
      .subscribe(resp => {
        if (resp) {
          resolve(resp);
        } else {
          reject();
        }
      });
    });
  }

  // traer doctor por id
  getDoctorId(id: number): Observable<Doctores>{
    return this.http.get<Doctores>(`${ URL }/Doctores/${ id }`);
  }

  // traer paciente por id de doctor
  getPacienteDoctorIdDoctor(id: number): Observable<ListaPacienteDoctor[]>{
    return this.http.get<ListaPacienteDoctor[]>(`${ URL }/PacienteDoctor/DoctorPacienteDoc/${ id }`);
  }
}
