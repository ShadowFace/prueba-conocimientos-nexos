import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pacientes } from '../interfaces/pacientes';
import { PacienteDoctor } from '../interfaces/paciente-doctor';
import { ListaPacienteDoctor } from '../interfaces/lista-paciente-doctor';

const URL = environment.URL;

@Injectable({
  providedIn: 'root'
})
export class PacientesService {

  constructor(private http: HttpClient) { }

  // traer todos los pacientes
  getPacientes(): Observable<Pacientes[]> {
    return this.http.get<Pacientes[]>(`${ URL }/Pacientes`);
  }

  // crear paciente
  // tslint:disable-next-line: typedef
  crearPaciente(paciente: Pacientes){
    return new Promise<Pacientes>((resolve, reject) => {
      this.http.post<Pacientes>(`${ URL }/Pacientes`, paciente)
      .subscribe(resp => {
        if (resp) {
          resolve(resp);
        } else {
          reject();
        }
      });
    });
  }

  // guardar doctor asociado a paciente
  crearPacienteDoctor(pacienteDoctor: PacienteDoctor): Observable<PacienteDoctor>{
    return this.http.post<PacienteDoctor>(`${ URL }/PacienteDoctor`, pacienteDoctor);
  }

  // eliminar doctor asociado a paciente
  // tslint:disable-next-line: typedef
  eliminarPacienteDoctor(id: number){
    return new Promise((resolve, reject) => {
      this.http.delete(`${ URL }/PacienteDoctor/BorrarPaciente/${ id }`)
      .subscribe(resp => {
        if (resp) {
          resolve(resp);
        } else {
          reject();
        }
      });
    });
  }

  // eliminar paciente
  // tslint:disable-next-line: typedef
  eliminarPaciente(id: number){
    return new Promise((resolve, reject) => {
      this.http.delete(`${ URL }/Pacientes/${ id }`)
      .subscribe(resp => {
        if (resp) {
          resolve(resp);
        } else {
          reject();
        }
      });
    });
  }

  // traer un paciente por id
  getPacienteId(id: number): Observable<Pacientes> {
    return this.http.get<Pacientes>(`${ URL }/Pacientes/${ id }`);
  }

  // traer doctor por id de paciente
  getPacienteDoctorIdPaciente(id: number): Observable<ListaPacienteDoctor[]>{
    return this.http.get<ListaPacienteDoctor[]>(`${ URL }/PacienteDoctor/DoctorPaciente/${ id }`);
  }

  // verifica doctor por id de paciente
  // tslint:disable-next-line: typedef
  getVerificarPacienteDoctorIdPaciente(id: number){
    return new Promise<ListaPacienteDoctor[]>((resolve) => {
      this.http.get<ListaPacienteDoctor[]>(`${ URL }/PacienteDoctor/DoctorPaciente/${ id }`)
      .subscribe(resp => {
        resolve(resp);
      });
    });
  }

  // actualizar paciente
  // tslint:disable-next-line: typedef
  actualizarPaciente(id: number, paciente: Pacientes){
    paciente.id = id;
    return new Promise<any>((resolve) => {
      this.http.put(`${ URL }/Pacientes/${ id }`, paciente).subscribe();
      resolve(true);
    });
  }

  // actualizar doctor asociado a paciente
  actualizarPacienteDoctor(pacienteDoctor: PacienteDoctor): Observable<any>{
    return this.http.put<any>(`${ URL }/PacienteDoctor/${ pacienteDoctor.id }`, pacienteDoctor);
  }

  // traer todos los pacientes con doctor
  getPacientesDoctores(): Observable<ListaPacienteDoctor[]>{
    return this.http.get<ListaPacienteDoctor[]>(`${ URL }/PacienteDoctor`);
  }
}
