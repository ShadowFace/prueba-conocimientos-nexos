export interface Doctores {
    id: number;
    nombre: string;
    cedula: string;
    especialidad: string;
    numeroCredencial: string;
    hospitalTrabajo: string;
    telefono: string;
}
