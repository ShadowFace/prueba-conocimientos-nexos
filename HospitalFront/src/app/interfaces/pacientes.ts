export interface Pacientes {
    id: number;
    nombre: string;
    cedula: string;
    numeroSeguro: string;
    codigoPostal: string;
    telefono: string;
}
