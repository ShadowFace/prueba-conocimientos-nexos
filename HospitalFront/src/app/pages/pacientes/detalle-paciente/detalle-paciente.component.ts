import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PacientesService } from '../../../services/pacientes.service';
import { Pacientes } from '../../../interfaces/pacientes';
import Swal from 'sweetalert2';
import { ListaPacienteDoctor } from '../../../interfaces/lista-paciente-doctor';

@Component({
  selector: 'app-detalle-paciente',
  templateUrl: './detalle-paciente.component.html',
  styleUrls: ['./detalle-paciente.component.css']
})
export class DetallePacienteComponent implements OnInit {

  titulo = 'Detalle paciente';
  paciente: Pacientes = {
    id: null,
    nombre: null,
    cedula: null,
    numeroSeguro: null,
    codigoPostal: null,
    telefono: null
  };
  idPaciente: number;
  doctores: ListaPacienteDoctor[] = [];

  constructor(private pacientesService: PacientesService,
              private route: ActivatedRoute,
              private router: Router) {
    this.idPaciente = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.getDoctoresPorPaciente(this.idPaciente);
  }

  ngOnInit(): void {
    this.getPaciente(this.idPaciente);
  }

  getPaciente(id: number): void{
    this.pacientesService.getPacienteId(id)
    .subscribe(resp => {
      this.paciente = resp;
    }, async (error) => {
      await Swal.fire('Error', error.error, 'error');
      this.router.navigateByUrl('pacientes');
    });
  }

  getDoctoresPorPaciente(id: number): void{
    this.pacientesService.getPacienteDoctorIdPaciente(id)
    .subscribe(resp => {
      this.doctores = resp;
    });
  }
}
