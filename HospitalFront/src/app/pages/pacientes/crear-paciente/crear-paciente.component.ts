import { Component, OnInit } from '@angular/core';
import { PacientesService } from '../../../services/pacientes.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DoctoresService } from '../../../services/doctores.service';
import { Doctores } from '../../../interfaces/doctores';
import { PacienteDoctor } from '../../../interfaces/paciente-doctor';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear-paciente',
  templateUrl: './crear-paciente.component.html',
  styleUrls: ['./crear-paciente.component.css']
})
export class CrearPacienteComponent implements OnInit {

  titulo = 'Crear paciente';
  mensaje = 'Campo obligatorio';
  formulario: FormGroup;
  doctores: Doctores[] = [];
  pacDoc: PacienteDoctor;

  constructor(private pacientesService: PacientesService,
              private doctoresService: DoctoresService,
              private fb: FormBuilder,
              private router: Router) {
    this.crearFormulario();
    this.cargarDoctores();
  }

  ngOnInit(): void {
  }

  get nombreObligatorio(): boolean{
    return this.formulario.get('Nombre').invalid && this.formulario.get('Nombre').touched;
  }

  get cedulaObligatorio(): boolean{
    return this.formulario.get('Cedula').invalid && this.formulario.get('Cedula').touched;
  }

  get seguroObligatorio(): boolean{
    return this.formulario.get('NumeroSeguro').invalid && this.formulario.get('NumeroSeguro').touched;
  }

  get postalObligatorio(): boolean{
    return this.formulario.get('CodigoPostal').invalid && this.formulario.get('CodigoPostal').touched;
  }

  get telefonoObligatorio(): boolean{
    return this.formulario.get('Telefono').invalid && this.formulario.get('Telefono').touched;
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      Nombre: ['', Validators.required],
      Cedula: ['', Validators.required],
      NumeroSeguro: ['', Validators.required],
      CodigoPostal: ['', Validators.required],
      Telefono: ['', Validators.required],
      Doctores: ['']
    });
  }

  async crearPaciente(): Promise<void>{
    if (this.formulario.invalid) {
      Object.values(this.formulario.controls).forEach(control => control.markAsTouched());
      return;
    }

    const guardar = await this.pacientesService.crearPaciente(this.formulario.value);

    if (guardar) {
      if (this.formulario.controls['Doctores'].value > 0) {
        this.pacDoc = {
          id: 0,
          FkIdPaciente: guardar.id,
          FkIdDoctor: this.formulario.controls['Doctores'].value
        };

        this.crearPacienteDoctor(this.pacDoc);
      }
      await Swal.fire('Creado', 'Paciente creado correctamente', 'success');
      this.router.navigateByUrl('pacientes');
    } else {
      await Swal.fire('Error', 'No se pudo crear el paciente');
      this.router.navigateByUrl('pacientes');
    }
  }

  cargarDoctores(): void{
    this.doctoresService.getDoctores()
    .subscribe(resp => {
      this.doctores = resp;
    });
  }

  crearPacienteDoctor(data: PacienteDoctor): void{
    this.pacientesService.crearPacienteDoctor(data).subscribe();
  }
}
