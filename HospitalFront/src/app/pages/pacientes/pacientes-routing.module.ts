import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PacientesComponent } from './pacientes.component';
import { ListarPacientesComponent } from './listar-pacientes/listar-pacientes.component';
import { CrearPacienteComponent } from './crear-paciente/crear-paciente.component';
import { ActualizarPacienteComponent } from './actualizar-paciente/actualizar-paciente.component';
import { DetallePacienteComponent } from './detalle-paciente/detalle-paciente.component';

const routes: Routes = [
  {
    path: 'pacientes', component: PacientesComponent,
    children: [
      {path: '', component: ListarPacientesComponent},
      {path: 'crear-paciente', component: CrearPacienteComponent},
      {path: 'actualizar-paciente/:id', component: ActualizarPacienteComponent},
      {path: 'detalle-paciente/:id', component: DetallePacienteComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacientesRoutingModule { }
