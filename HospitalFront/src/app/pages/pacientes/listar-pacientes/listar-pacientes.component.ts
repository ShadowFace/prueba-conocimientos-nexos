import { Component, OnInit } from '@angular/core';
import { PacientesService } from '../../../services/pacientes.service';
import { Pacientes } from '../../../interfaces/pacientes';
import { PacienteDoctor } from '../../../interfaces/paciente-doctor';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ListaPacienteDoctor } from '../../../interfaces/lista-paciente-doctor';

@Component({
  selector: 'app-listar-pacientes',
  templateUrl: './listar-pacientes.component.html',
  styleUrls: ['./listar-pacientes.component.css']
})
export class ListarPacientesComponent implements OnInit {

  doctores: ListaPacienteDoctor[] = [];
  pacientes: Pacientes[] = [];

  constructor(private pacientesService: PacientesService,
              private router: Router) { }

  ngOnInit(): void {
    this.cargarPacientes();
  }

  cargarPacientes(): void{
    this.pacientesService.getPacientes()
    .subscribe(resp => {
      this.pacientes = resp;
    });
  }

  getDoctoresPorPaciente(id: number): void{
    this.pacientesService.getPacienteDoctorIdPaciente(id)
    .subscribe(resp => {
      this.doctores = resp;
    });
  }

  eliminarPaciente(paciente: Pacientes): void{
    Swal.fire({
      title: '¿Está seguro?',
      text: `Está seguro que desea eliminar el paciente ${ paciente.nombre }`,
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then(async (resp) => {
      if (resp.value) {
        const verificar = await this.pacientesService.getVerificarPacienteDoctorIdPaciente(paciente.id);

        if (verificar.length > 0) {
          const eliminar = await this.pacientesService.eliminarPacienteDoctor(paciente.id);

          if (eliminar) {
            const dato = await this.pacientesService.eliminarPaciente(paciente.id);

            if (dato) {
              await Swal.fire('Eliminado', 'Paciente eliminado correctamente', 'success');
              this.cargarPacientes();
            } else {
              await Swal.fire('Error', 'No se pudo eliminar el paciente');
            }
          } else {
            await Swal.fire('Error', 'No se pudo eliminar el paciente');
          }
        } else {
          const dato = await this.pacientesService.eliminarPaciente(paciente.id);

          if (dato) {
            await Swal.fire('Eliminado', 'Paciente eliminado correctamente', 'success');
            this.cargarPacientes();
          } else {
            await Swal.fire('Error', 'No se pudo eliminar el paciente');
          }
        }
      }
    });
  }
}
