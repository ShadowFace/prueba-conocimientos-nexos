import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Doctores } from 'src/app/interfaces/doctores';
import { PacienteDoctor } from 'src/app/interfaces/paciente-doctor';
import { DoctoresService } from 'src/app/services/doctores.service';
import { PacientesService } from 'src/app/services/pacientes.service';
import { Pacientes } from '../../../interfaces/pacientes';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-actualizar-paciente',
  templateUrl: './actualizar-paciente.component.html',
  styleUrls: ['./actualizar-paciente.component.css']
})
export class ActualizarPacienteComponent implements OnInit {

  titulo = 'Editar paciente';
  mensaje = 'Campo obligatorio';
  formulario: FormGroup;
  doctores: Doctores[] = [];
  pacDoc: PacienteDoctor;
  paciente: Pacientes;
  idPaciente: number;

  constructor(private pacientesService: PacientesService,
              private doctoresService: DoctoresService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) {
    this.crearFormulario();
    this.cargarDoctores();
  }

  ngOnInit(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.idPaciente = id;

    this.pacientesService.getPacienteId(id)
    .subscribe(resp => {
      this.paciente = resp;
      this.cargarFormulario();
    }, async (error) => {
      await Swal.fire('Error', error.error, 'error');
      this.router.navigateByUrl('pacientes');
    });

    this.getPacienteDoctorId(id);
  }

  get nombreObligatorio(): boolean{
    return this.formulario.get('Nombre').invalid && this.formulario.get('Nombre').touched;
  }

  get cedulaObligatorio(): boolean{
    return this.formulario.get('Cedula').invalid && this.formulario.get('Cedula').touched;
  }

  get seguroObligatorio(): boolean{
    return this.formulario.get('NumeroSeguro').invalid && this.formulario.get('NumeroSeguro').touched;
  }

  get postalObligatorio(): boolean{
    return this.formulario.get('CodigoPostal').invalid && this.formulario.get('CodigoPostal').touched;
  }

  get telefonoObligatorio(): boolean{
    return this.formulario.get('Telefono').invalid && this.formulario.get('Telefono').touched;
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      Nombre: ['', Validators.required],
      Cedula: ['', Validators.required],
      NumeroSeguro: ['', Validators.required],
      CodigoPostal: ['', Validators.required],
      Telefono: ['', Validators.required],
      Doctores: ['']
    });
  }

  cargarFormulario(): void{
    this.formulario.controls['Nombre'].setValue(this.paciente.nombre);
    this.formulario.controls['Cedula'].setValue(this.paciente.cedula);
    this.formulario.controls['NumeroSeguro'].setValue(this.paciente.numeroSeguro);
    this.formulario.controls['CodigoPostal'].setValue(this.paciente.codigoPostal);
    this.formulario.controls['Telefono'].setValue(this.paciente.telefono);
  }

  async actualizarPaciente(): Promise<void>{
    if (this.formulario.invalid) {
      Object.values(this.formulario.controls).forEach(control => control.markAsTouched());
      return;
    }

    const actualizar = await this.pacientesService.actualizarPaciente(this.idPaciente, this.formulario.value);

    if (actualizar) {
      if (this.formulario.controls['Doctores'].value > 0) {
        const dato = this.pacDoc.id;

        this.pacDoc = {
          id: dato,
          FkIdPaciente: this.idPaciente,
          FkIdDoctor: this.formulario.controls['Doctores'].value
        };

        if (dato === 0) {
          this.crearPacienteDoctor(this.pacDoc);
        } else {
          this.actualizarPacienteDoctor(this.pacDoc);
        }
      }
      await Swal.fire('Actualizado', 'Paciente actualizado correctamente', 'success');
      this.router.navigateByUrl('pacientes');
    } else {
      await Swal.fire('Error', 'No se pudo actualizar el paciente');
      this.router.navigateByUrl('pacientes');
    }
  }

  cargarDoctores(): void{
    this.doctoresService.getDoctores()
    .subscribe(resp => {
      this.doctores = resp;
    });
  }

  actualizarPacienteDoctor(data: PacienteDoctor): void{
    this.pacientesService.actualizarPacienteDoctor(data).subscribe();
  }

  crearPacienteDoctor(data: PacienteDoctor): void{
    this.pacientesService.crearPacienteDoctor(data).subscribe();
  }

  getPacienteDoctorId(id: number): void{
    this.pacientesService.getPacienteDoctorIdPaciente(id)
    .subscribe(resp => {
      if (resp.length > 0) {
        this.pacDoc = resp[0];
        this.formulario.controls['Doctores'].setValue(this.pacDoc.FkIdDoctor);
      } else {
        this.pacDoc = {
          id: 0,
          FkIdPaciente: null,
          FkIdDoctor: null
        };
      }
    });
  }
}
