import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { PacientesRoutingModule } from './pacientes-routing.module';

import { PacientesComponent } from './pacientes.component';
import { ListarPacientesComponent } from './listar-pacientes/listar-pacientes.component';
import { CrearPacienteComponent } from './crear-paciente/crear-paciente.component';
import { ActualizarPacienteComponent } from './actualizar-paciente/actualizar-paciente.component';
import { DetallePacienteComponent } from './detalle-paciente/detalle-paciente.component';


@NgModule({
  declarations: [
    PacientesComponent,
    ListarPacientesComponent,
    CrearPacienteComponent,
    ActualizarPacienteComponent,
    DetallePacienteComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PacientesRoutingModule
  ]
})
export class PacientesModule { }
