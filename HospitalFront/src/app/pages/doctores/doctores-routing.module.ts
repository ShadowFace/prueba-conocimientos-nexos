import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearDoctorComponent } from './crear-doctor/crear-doctor.component';
import { DetalleDoctorComponent } from './detalle-doctor/detalle-doctor.component';
import { DoctoresComponent } from './doctores.component';
import { ListarDoctoresComponent } from './listar-doctores/listar-doctores.component';

const routes: Routes = [
  {
    path: 'doctores', component: DoctoresComponent,
    children: [
      {path: '', component: ListarDoctoresComponent},
      {path: 'crear-doctor', component: CrearDoctorComponent},
      {path: 'detalle-doctor/:id', component: DetalleDoctorComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctoresRoutingModule { }
