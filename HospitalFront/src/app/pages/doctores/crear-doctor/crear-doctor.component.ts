import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PacienteDoctor } from 'src/app/interfaces/paciente-doctor';
import { DoctoresService } from 'src/app/services/doctores.service';
import { PacientesService } from 'src/app/services/pacientes.service';
import { Pacientes } from '../../../interfaces/pacientes';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear-doctor',
  templateUrl: './crear-doctor.component.html',
  styleUrls: ['./crear-doctor.component.css']
})
export class CrearDoctorComponent implements OnInit {

  titulo = 'Crear doctor';
  mensaje = 'Campo obligatorio';
  formulario: FormGroup;
  pacientes: Pacientes[] = [];
  pacDoc: PacienteDoctor;

  constructor(private pacientesService: PacientesService,
              private doctoresService: DoctoresService,
              private fb: FormBuilder,
              private router: Router) {
    this.crearFormulario();
    this.cargarPacientes();
  }

  ngOnInit(): void {
  }

  get nombreObligatorio(): boolean{
    return this.formulario.get('Nombre').invalid && this.formulario.get('Nombre').touched;
  }

  get cedulaObligatorio(): boolean{
    return this.formulario.get('Cedula').invalid && this.formulario.get('Cedula').touched;
  }

  get especialidadObligatorio(): boolean{
    return this.formulario.get('Especialidad').invalid && this.formulario.get('Especialidad').touched;
  }

  get credencialObligatorio(): boolean{
    return this.formulario.get('NumeroCredencial').invalid && this.formulario.get('NumeroCredencial').touched;
  }

  get hospitalObligatorio(): boolean{
    return this.formulario.get('HospitalTrabajo').invalid && this.formulario.get('HospitalTrabajo').touched;
  }

  get telefonoObligatorio(): boolean{
    return this.formulario.get('Telefono').invalid && this.formulario.get('Telefono').touched;
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      Nombre: ['', Validators.required],
      Cedula: ['', Validators.required],
      Especialidad: ['', Validators.required],
      NumeroCredencial: ['', Validators.required],
      HospitalTrabajo: ['', Validators.required],
      Telefono: ['', Validators.required],
      Pacientes: ['']
    });
  }

  async crearDoctor(): Promise<void>{
    if (this.formulario.invalid) {
      Object.values(this.formulario.controls).forEach(control => control.markAsTouched());
      return;
    }

    const guardar = await this.doctoresService.crearDoctor(this.formulario.value);

    if (guardar) {
      if (this.formulario.controls['Pacientes'].value > 0) {
        this.pacDoc = {
          id: 0,
          FkIdPaciente: this.formulario.controls['Pacientes'].value,
          FkIdDoctor: guardar.id
        };

        this.crearPacienteDoctor(this.pacDoc);
      }
      await Swal.fire('Creado', 'Doctor creado correctamente', 'success');
      this.router.navigateByUrl('doctores');
    } else {
      await Swal.fire('Error', 'No se pudo crear el paciente');
      this.router.navigateByUrl('doctores');
    }
  }

  cargarPacientes(): void{
    this.doctoresService.getPacientes()
    .subscribe(resp => {
      this.pacientes = resp;
    });
  }

  crearPacienteDoctor(data: PacienteDoctor): void{
    this.pacientesService.crearPacienteDoctor(data).subscribe();
  }
}
