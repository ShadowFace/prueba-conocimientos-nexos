import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DoctoresService } from '../../../services/doctores.service';
import { Doctores } from '../../../interfaces/doctores';
import Swal from 'sweetalert2';
import { ListaPacienteDoctor } from '../../../interfaces/lista-paciente-doctor';

@Component({
  selector: 'app-detalle-doctor',
  templateUrl: './detalle-doctor.component.html',
  styleUrls: ['./detalle-doctor.component.css']
})
export class DetalleDoctorComponent implements OnInit {

  titulo = 'Detalle doctor';
  doctor: Doctores = {
    id: null,
    nombre: null,
    cedula: null,
    especialidad: null,
    numeroCredencial: null,
    hospitalTrabajo: null,
    telefono: null
  };
  idDoctor: number;
  pacientes: ListaPacienteDoctor[] = [];

  constructor(private doctoresService: DoctoresService,
              private route: ActivatedRoute,
              private router: Router) {
    this.idDoctor = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.getPacientesPorDoctor(this.idDoctor);
  }

  ngOnInit(): void {
    this.getDoctor(this.idDoctor);
  }

  getDoctor(id: number): void{
    this.doctoresService.getDoctorId(id)
    .subscribe(resp => {
      this.doctor = resp;
    }, async (error) => {
      await Swal.fire('Error', error.error, 'error');
      this.router.navigateByUrl('doctores');
    });
  }

  getPacientesPorDoctor(id: number): void{
    this.doctoresService.getPacienteDoctorIdDoctor(id)
    .subscribe(resp => {
      this.pacientes = resp;
    });
  }
}
