import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctoresRoutingModule } from './doctores-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { DoctoresComponent } from './doctores.component';
import { ListarDoctoresComponent } from './listar-doctores/listar-doctores.component';
import { CrearDoctorComponent } from './crear-doctor/crear-doctor.component';
import { DetalleDoctorComponent } from './detalle-doctor/detalle-doctor.component';


@NgModule({
  declarations: [
    DoctoresComponent,
    ListarDoctoresComponent,
    CrearDoctorComponent,
    DetalleDoctorComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DoctoresRoutingModule
  ]
})
export class DoctoresModule { }
