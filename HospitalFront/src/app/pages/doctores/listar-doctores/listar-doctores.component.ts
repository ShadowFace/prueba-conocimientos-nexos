import { Component, OnInit } from '@angular/core';
import { Doctores } from '../../../interfaces/doctores';
import { DoctoresService } from '../../../services/doctores.service';
import { ListaPacienteDoctor } from '../../../interfaces/lista-paciente-doctor';

@Component({
  selector: 'app-listar-doctores',
  templateUrl: './listar-doctores.component.html',
  styleUrls: ['./listar-doctores.component.css']
})
export class ListarDoctoresComponent implements OnInit {

  doctores: Doctores[] = [];
  pacientes: ListaPacienteDoctor[] = [];

  constructor(private doctoresService: DoctoresService) { }

  ngOnInit(): void {
    this.cargarDoctores();
  }

  cargarDoctores(): void{
    this.doctoresService.getDoctores()
    .subscribe(resp => {
      this.doctores = resp;
    });
  }

  getPacientesPorDoctor(id: number): void{
    this.doctoresService.getPacienteDoctorIdDoctor(id)
    .subscribe(resp => {
      this.pacientes = resp;
    });
  }
}
