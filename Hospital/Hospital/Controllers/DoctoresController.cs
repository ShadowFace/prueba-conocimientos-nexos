﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctoresController : ControllerBase
    {
        private readonly HospitalContext _context;

        public DoctoresController(HospitalContext context)
        {
            _context = context;
        }

        // GET: api/Doctores
        [HttpGet]
        public IEnumerable<Doctor> GetDoctor()
        {
            return _context.Doctor;
        }

        // GET: api/Doctores/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDoctor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var doctor = await _context.Doctor.FindAsync(id);

            if (doctor == null)
            {
                return NotFound("Doctor no existe en base de datos");
            }

            return Ok(doctor);
        }

        // POST: api/Doctores
        [HttpPost]
        public async Task<IActionResult> PostDoctor([FromBody] Doctor doctor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Doctor.Add(doctor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDoctor", new { id = doctor.Id }, doctor);
        }
    }
}