﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PacienteDoctorController : ControllerBase
    {
        private readonly HospitalContext _context;

        public PacienteDoctorController(HospitalContext context)
        {
            _context = context;
        }

        // GET: api/PacienteDoctor
        [HttpGet]
        public IEnumerable<ListaPacienteDoctor> GetPacienteDoctor()
        {
            List<ListaPacienteDoctor> lista;

            lista = (from pac in _context.Paciente
                     join pacdoc in _context.PacienteDoctor on pac.Id equals pacdoc.FkIdPaciente
                     join doct in _context.Doctor on pacdoc.FkIdDoctor equals doct.Id
                     select new ListaPacienteDoctor
                     {
                         Id = pacdoc.Id,
                         FkIdPaciente = pacdoc.FkIdPaciente,
                         FkIdDoctor = pacdoc.FkIdDoctor,
                         Paciente = pac.Nombre,
                         Doctor = doct.Nombre
                     }).ToList();

            return lista;
        }

        // GET: api/PacienteDoctor/5
        [HttpGet("{id}")]
        public IEnumerable<ListaPacienteDoctor> GetPacienteDoctor([FromRoute] int id)
        {
            var pacienteDoctor = (from pac in _context.Paciente
                                  join pacdoc in _context.PacienteDoctor on pac.Id equals pacdoc.FkIdPaciente
                                  join doct in _context.Doctor on pacdoc.FkIdDoctor equals doct.Id
                                  where pacdoc.Id == id
                                  select new ListaPacienteDoctor
                                  {
                                      Id = pacdoc.Id,
                                      FkIdPaciente = pacdoc.FkIdPaciente,
                                      FkIdDoctor = pacdoc.FkIdDoctor,
                                      Paciente = pac.Nombre,
                                      Doctor = doct.Nombre
                                  }).ToList();

            return pacienteDoctor;
        }

        // GET: api/PacienteDoctor/DoctorPaciente/5
        [HttpGet("DoctorPaciente/{id}")]
        public IEnumerable<ListaPacienteDoctor> GetPacienteDoctorIdPaciente([FromRoute] int id)
        {
            var pacienteDoctor = (from pac in _context.Paciente
                                  join pacdoc in _context.PacienteDoctor on pac.Id equals pacdoc.FkIdPaciente
                                  join doct in _context.Doctor on pacdoc.FkIdDoctor equals doct.Id
                                  where pacdoc.FkIdPaciente == id
                                  select new ListaPacienteDoctor
                                  {
                                      Id = pacdoc.Id,
                                      FkIdPaciente = pacdoc.FkIdPaciente,
                                      FkIdDoctor = pacdoc.FkIdDoctor,
                                      Paciente = pac.Nombre,
                                      Doctor = doct.Nombre
                                  }).ToList();

            return pacienteDoctor;
        }

        // GET: api/PacienteDoctor/DoctorPacienteDoc/5
        [HttpGet("DoctorPacienteDoc/{id}")]
        public IEnumerable<ListaPacienteDoctor> GetPacienteDoctorIdDoctor([FromRoute] int id)
        {
            var pacienteDoctor = (from pac in _context.Paciente
                                  join pacdoc in _context.PacienteDoctor on pac.Id equals pacdoc.FkIdPaciente
                                  join doct in _context.Doctor on pacdoc.FkIdDoctor equals doct.Id
                                  where pacdoc.FkIdDoctor == id
                                  select new ListaPacienteDoctor
                                  {
                                      Id = pacdoc.Id,
                                      FkIdPaciente = pacdoc.FkIdPaciente,
                                      FkIdDoctor = pacdoc.FkIdDoctor,
                                      Paciente = pac.Nombre,
                                      Doctor = doct.Nombre
                                  }).ToList();

            return pacienteDoctor;
        }

        // PUT: api/PacienteDoctor/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPacienteDoctor([FromRoute] int id, [FromBody] PacienteDoctor pacienteDoctor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pacienteDoctor.Id)
            {
                return BadRequest();
            }

            _context.Entry(pacienteDoctor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PacienteDoctorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PacienteDoctor
        [HttpPost]
        public async Task<IActionResult> PostPacienteDoctor([FromBody] PacienteDoctor pacienteDoctor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PacienteDoctor.Add(pacienteDoctor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPacienteDoctor", new { id = pacienteDoctor.Id }, pacienteDoctor);
        }

        // DELETE: api/PacienteDoctor/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePacienteDoctor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pacienteDoctor = await _context.PacienteDoctor.FindAsync(id);
            if (pacienteDoctor == null)
            {
                return NotFound();
            }

            _context.PacienteDoctor.Remove(pacienteDoctor);
            await _context.SaveChangesAsync();

            return Ok(pacienteDoctor);
        }

        // DELETE: api/PacienteDoctor/BorrarPaciente/5
        [HttpDelete("BorrarPaciente/{id}")]
        public async Task<IActionResult> BorrarPacPacienteDoctor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pacienteDoctor = await _context.PacienteDoctor.Where(d => d.FkIdPaciente == id).ToListAsync();

            if (pacienteDoctor.Count == 0)
            {
                return NotFound();
            }

            foreach(var item in pacienteDoctor)
            {
                _context.PacienteDoctor.Remove(item);
                await _context.SaveChangesAsync();
            }

            return Ok(pacienteDoctor);
        }

        // DELETE: api/PacienteDoctor/BorrarDoctor/5
        [HttpDelete("BorrarDoctor/{id}")]
        public async Task<IActionResult> BorrarDocPacienteDoctor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pacienteDoctor = await _context.PacienteDoctor.Where(d => d.FkIdDoctor == id).ToListAsync();

            if (pacienteDoctor.Count == 0)
            {
                return NotFound();
            }

            foreach (var item in pacienteDoctor)
            {
                _context.PacienteDoctor.Remove(item);
                await _context.SaveChangesAsync();
            }

            return Ok(pacienteDoctor);
        }

        private bool PacienteDoctorExists(int id)
        {
            return _context.PacienteDoctor.Any(e => e.Id == id);
        }
    }
}