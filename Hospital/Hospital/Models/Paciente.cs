﻿using System;
using System.Collections.Generic;

namespace Hospital.Models
{
    public partial class Paciente
    {
        public Paciente()
        {
            PacienteDoctor = new HashSet<PacienteDoctor>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string NumeroSeguro { get; set; }
        public string CodigoPostal { get; set; }
        public string Telefono { get; set; }

        public ICollection<PacienteDoctor> PacienteDoctor { get; set; }
    }
}
