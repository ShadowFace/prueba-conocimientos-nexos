﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Hospital.Models
{
    public partial class HospitalContext : DbContext
    {
        public HospitalContext()
        {
        }

        public HospitalContext(DbContextOptions<HospitalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Doctor> Doctor { get; set; }
        public virtual DbSet<Paciente> Paciente { get; set; }
        public virtual DbSet<PacienteDoctor> PacienteDoctor { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Doctor>(entity =>
            {
                entity.ToTable("DOCTOR");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Cedula)
                    .IsRequired()
                    .HasColumnName("CEDULA")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Especialidad)
                    .IsRequired()
                    .HasColumnName("ESPECIALIDAD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HospitalTrabajo)
                    .IsRequired()
                    .HasColumnName("HOSPITAL_TRABAJO")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroCredencial)
                    .IsRequired()
                    .HasColumnName("NUMERO_CREDENCIAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .IsRequired()
                    .HasColumnName("TELEFONO")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Paciente>(entity =>
            {
                entity.ToTable("PACIENTE");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Cedula)
                    .IsRequired()
                    .HasColumnName("CEDULA")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoPostal)
                    .IsRequired()
                    .HasColumnName("CODIGO_POSTAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroSeguro)
                    .IsRequired()
                    .HasColumnName("NUMERO_SEGURO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .IsRequired()
                    .HasColumnName("TELEFONO")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PacienteDoctor>(entity =>
            {
                entity.ToTable("PACIENTE_DOCTOR");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FkIdDoctor).HasColumnName("FK_ID_DOCTOR");

                entity.Property(e => e.FkIdPaciente).HasColumnName("FK_ID_PACIENTE");

                entity.HasOne(d => d.FkIdDoctorNavigation)
                    .WithMany(p => p.PacienteDoctor)
                    .HasForeignKey(d => d.FkIdDoctor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ID_DOC");

                entity.HasOne(d => d.FkIdPacienteNavigation)
                    .WithMany(p => p.PacienteDoctor)
                    .HasForeignKey(d => d.FkIdPaciente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ID_PA");
            });
        }
    }
}
