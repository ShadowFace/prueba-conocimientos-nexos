﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Models
{
    public class ListaPacienteDoctor
    {
        public int Id { get; set; }
        public int FkIdPaciente { get; set; }
        public int FkIdDoctor { get; set; }
        public string Paciente { get; set; }
        public string Doctor { get; set; }
    }
}
