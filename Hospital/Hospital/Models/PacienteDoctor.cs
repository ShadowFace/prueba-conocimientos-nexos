﻿using System;
using System.Collections.Generic;

namespace Hospital.Models
{
    public partial class PacienteDoctor
    {
        public int Id { get; set; }
        public int FkIdPaciente { get; set; }
        public int FkIdDoctor { get; set; }

        public Doctor FkIdDoctorNavigation { get; set; }
        public Paciente FkIdPacienteNavigation { get; set; }
    }
}
