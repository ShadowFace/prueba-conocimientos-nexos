﻿using System;
using System.Collections.Generic;

namespace Hospital.Models
{
    public partial class Doctor
    {
        public Doctor()
        {
            PacienteDoctor = new HashSet<PacienteDoctor>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string Especialidad { get; set; }
        public string NumeroCredencial { get; set; }
        public string HospitalTrabajo { get; set; }
        public string Telefono { get; set; }

        public ICollection<PacienteDoctor> PacienteDoctor { get; set; }
    }
}
